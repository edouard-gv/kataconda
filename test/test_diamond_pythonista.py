from diamond_pythonista import diamond

# import expects

EXPECTED_B_RESULT = """
 A
B B
 A
"""


def test_diamond_with_a():
    assert diamond("A") == "A"


def test_diamond_with_b():
    assert diamond("B") == EXPECTED_B_RESULT.strip("\n")


# Using expects or rober assertion library
#     expect(diamond('B')).to.equal(EXPECTED_B_RESULT)

# Using assert with mamba
# with describe('diamond'):
#     with it('returns the expected result when called with A'):
#         assert diamond("A") == "A"
#     with it('returns the expected result when called with A'):
#         assert diamond("B") == EXPECTED_B_RESULT
