"""
Petit module pour tester quelques fonctions
"""


def count_char(param):
    # Déjà implémenté dans https://docs.python.org/3/library/collections.html#counter-objects
    d = dict()
    for c in param:
        if c in d:
            d[c] = d[c] + 1
        else:
            d[c] = 1
    return d


def test_count_char_empty():
    assert len(count_char("")) == 0


def test_count_char_one():
    assert count_char("a")["a"] == 1


def test_count_char_multiple():
    assert count_char("aa")["a"] == 2


def test_count_char_several_one():
    d = count_char("abc")
    assert d["a"] == 1
    assert d["b"] == 1
    assert d["c"] == 1


def test_count_char_several_multiple():
    d = count_char("aa bc cdea")
    assert d["a"] == 3
    assert d["b"] == 1
    assert d["c"] == 2
