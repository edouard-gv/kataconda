import itertools

from game_of_life import (
    generator,
    GameOfLife,
    neighbours,
    ascii_print,
)


def test_GameOfLife_tick():
    seed = set()
    game = GameOfLife(seed)
    next_generation = game.tick()
    assert next_generation == set()


def test_GameOfLife_tick_with_one_death():
    seed = {(0, 0)}
    game = GameOfLife(seed)
    game.tick()
    assert game.alive_cells == set()


def test_GameOfLife_tick_with_one_survivor():
    """
    stable foursome
    .**.
    .**.
    """
    seed = {(1, 0), (2, 0), (1, 1), (2, 1)}
    game = GameOfLife(seed)
    game.tick()
    assert game.alive_cells == seed


def test_blinker_generations():
    """
    guiding test with a blinker where some cells die/survive/born:

    .....
    .***.
    .....

    ->

    ..*..
    ..*..
    ..*..
    """
    seed = {(1, 1), (2, 1), (3, 1)}
    game = generator(seed)
    assert next(game) == {(2, 0), (2, 1), (2, 2)}
    assert next(game) == seed


def test_generations():
    seed = set()
    game = generator(seed)
    assert next(game) == set()


def test_GameOfLife_live_neighbours_with_no_live_neighbours():
    """
    .*.
    """

    live_neighbours = GameOfLife({(1, 0)}).live_neighbours((1, 0))
    assert live_neighbours == set()


def test_GameOfLife_live_neighbours_with_one_live_neighbours():
    """
    .**.
    """

    live_neighbours = GameOfLife({(1, 0), (2, 0)}).live_neighbours((1, 0))
    assert live_neighbours == {(2, 0)}


def test_GameOfLife_births():
    """
    .*
    **
    """

    seed = {(1, 0), (0, 1), (1, 1)}
    game = GameOfLife(seed)
    assert game.births() == {(0, 0)}


def test_GameOfLife_births_candidates():
    """
    *.
    """

    seed = {(0, 0)}
    game = GameOfLife(seed)
    assert game.birth_candidates() == set(neighbours((0, 0)))


def test_neighbours_at_origin():
    my_neighbours = neighbours((0, 0))
    assert len(my_neighbours) == 8
    assert (0, 0) not in my_neighbours


def test_neighbours():
    my_neighbours = neighbours((1, 0))
    assert len(my_neighbours) == 8
    assert (1, 0) not in my_neighbours
    assert (0, 0) in my_neighbours
    assert (0, 1) in my_neighbours


def test_ascii_print():
    expected = """···
···
·*·
···
"""
    assert ascii_print({(1, 2)}, 3, 4) == expected


def test_numeric_neighbours():
    assert merge_neighbours({2, 3, 7}) == {1, 2, 3, 4, 6, 7, 8}


def test_numeric_neighbours_empty():
    assert merge_neighbours({}) == set()


def numeric_neighbours(n):
    return {n - 1, n, n + 1}


def merge_neighbours(a_set_of_numeric):
    return set(
        itertools.chain.from_iterable(numeric_neighbours(n) for n in a_set_of_numeric)
    )
