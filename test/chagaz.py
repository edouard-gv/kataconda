import math
import string

"""
Petit module pour tester quelques fonctions
"""


def test_7():
    assert first_power(7) == 8


def test_8():
    assert first_power(8) == 8


def test_9():
    assert first_power(9) == 16


def first_power(n):
    return 2 ** math.ceil(math.log2(n))


def compute_substring(s):
    return s[2::3]


def test_slice():
    assert compute_substring(string.ascii_lowercase) == "cfilorux"
