import unittest

from diamond import Printer


class MyTestCase(unittest.TestCase):
    def test_caseA(self):
        printer = Printer("A")
        self.assertEqual("A", printer.lines[0])
        self.assertEqual("A", printer.text())

    def test_caseB(self):
        printer = Printer("B")
        print("\n===\n" + printer.text() + "\n===")
        self.assertEqual(" A", printer.lines[0])
        self.assertEqual("B B", printer.lines[1])
        self.assertEqual(" A", printer.lines[2])

    def test_caseC(self):
        printer = Printer("C")
        print("\n===\n" + printer.text() + "\n===")
        self.assertEqual("  A", printer.lines[0])
        self.assertEqual("C   C", printer.lines[2])
        self.assertEqual("  A", printer.lines[4])

    def _test_test(self):
        for i in range(10):
            print("-" * i + str(i))


if __name__ == "__main__":
    unittest.main()
