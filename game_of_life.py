import itertools
import time


def ascii_print(alive_cells, x_max, y_max):
    table = ""
    for y in range(y_max):
        line = ""
        for x in range(x_max):
            line = line + ("*" if (x, y) in alive_cells else "·")
        table = table + line + "\n"
    return table


def generator(seed):
    game = GameOfLife(seed)
    while True:
        yield game.tick()


class GameOfLife(object):
    def __init__(self, seed):
        self.alive_cells = seed

    def tick(self):
        self.alive_cells = set.union(self.survivors(), self.births())
        return self.alive_cells

    def survivors(self):
        return {
            cell
            for cell in self.alive_cells
            if len(self.live_neighbours(cell)) in [2, 3]
        }

    def births(self):
        return {
            candidate
            for candidate in self.birth_candidates()
            if len(self.live_neighbours(candidate)) == 3
        }

    def birth_candidates(self):
        return set(
            itertools.chain.from_iterable(neighbours(cell) for cell in self.alive_cells)
        )

    def live_neighbours(self, cell):
        return set.intersection(neighbours(cell), self.alive_cells)


def neighbours(cell):
    # fmt: off
    deltas = [(-1, -1), (0, -1), (1, -1),
              (-1, 0),           (1, 0),
              (-1, 1),  (0, 1),  (1, 1)]
    # fmt: on
    x, y = cell
    return {(x + dx, y + dy) for (dx, dy) in deltas}


if __name__ == "__main__":
    SEED = {(1, 1), (2, 1), (3, 1)}
    X_MAX = 10
    Y_MAX = 10
    GAME = generator(SEED)
    ALIVE_CELLS = SEED
    while True:
        print(ascii_print(ALIVE_CELLS, X_MAX, Y_MAX))
        ALIVE_CELLS = next(GAME)
        time.sleep(1)
