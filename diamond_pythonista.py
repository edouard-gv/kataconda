from functools import reduce


def diamond(letter):
    offset = ord("A")
    ord_target = ord(letter) - offset
    lines = []
    for i in range(ord_target + 1):
        lines.append(
            " " * (ord_target - i)
            + chr(i + offset)
            + (" " * (i * 2 - 1) + chr(i + offset) if i != 0 else "")
        )
    for i in range(ord_target):
        lines.append(lines[ord_target - i - 1])
    return "\n".join(lines)
