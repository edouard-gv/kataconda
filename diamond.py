from functools import reduce


class Printer:
    def __init__(self, target):
        offset = ord("A")
        ord_target = ord(target) - offset
        self.lines = []
        for i in range(ord_target + 1):
            self.lines.append(
                " " * (ord_target - i)
                + chr(i + offset)
                + (" " * (i * 2 - 1) + chr(i + offset) if i != 0 else "")
            )
        for i in range(ord_target):
            self.lines.append(self.lines[ord_target - i - 1])

    def text(self):
        return "\n".join(self.lines)
